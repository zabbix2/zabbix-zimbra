#!/bin/bash

# Prepare
cd "$(dirname "${BASH_SOURCE[0]}")"
mkdir -p result

# Get queue
queue=$(su zimbra -c "/opt/zimbra/common/sbin/mailq" | tail -n 1)

# Get message count in queue
check_empty=$(echo "${queue}" | grep empty | wc -l)
if [ "$check_empty" -eq "0" ]; then
  queue_size=$(echo "${queue}" | awk '{print $5}')
else
  queue_size=0
fi
echo $queue_size > result/queue_size
