#!/bin/bash

# Prepare
cd "$(dirname "${BASH_SOURCE[0]}")"
mkdir -p result

# Begin discovery file
echo "{\"data\":[" > result/zbxdiscovery

# Get full info about all zimbra services
full_info=$(su zimbra -c "/opt/zimbra/bin/zmcontrol status" | grep -v Host | sed 's/ //')

# Get Zimbra services
services=$(echo "${full_info}" | awk '{print $1}')

# Get Zimbra services state
for service in $(echo "${services}"); do
  service_name=$(echo "${service}" | sed 's/ //g')
  service_state=$(echo "${full_info}" | grep $service | sed -e 's/.*/\L&/' | awk '{print $2}' | sed 's/ //g')
  if [ "$service_state" = "running" ]; then
    state=1
  else
    state=0
  fi
  echo "{ \"{#ZIMBRASERVICENAME}\":\"$service_name\" }, " >> result/zbxdiscovery
  echo $state > result/state_${service_name}
done

# Finish discovery file
echo "]}" >> result/zbxdiscovery
zbxdiscoverytmp=`cat result/zbxdiscovery | tr -d '\r\n' | sed 's/\(.*\),/\1/'`
echo $zbxdiscoverytmp > result/zbxdiscovery
